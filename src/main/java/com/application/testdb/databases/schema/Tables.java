/*
 Copyright (C) 2015 Enrico Bianchi (enrico.bianchi@ymail.com)
 Project       TestDB
 Description   Simple database testing tool
 License       GPL version 2 (see GPL.txt for details)
 */
package com.application.testdb.databases.schema;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.Month;

/**
 *
 * @author enrico
 */
public class Tables {

    private Connection conn;
    private Integer segments;

    public Tables(Connection conn, Integer segments) {
        this.conn = conn;
        this.segments = segments;
    }

    public Boolean checkSchema() throws SQLException {
        DatabaseMetaData metadata;
        Integer counted;

        counted = 0;
        metadata = this.conn.getMetaData();
        try (ResultSet res = metadata.getTables(null, null, "%", new String[]{"TABLE"});) {
            while (res.next()) {
                counted++;
                break;
            }
        }
        if (counted > 0) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public void generate() throws SQLException {
        String cols = "ins      INTEGER,\n"
                + "date_ins INTEGER,\n"
                + "hour_ins INTEGER,\n"
                + "txt      VARCHAR(30)";

        try (Statement stmt = this.conn.createStatement();) {
            for (Integer i = 1; this.segments >= i; i++) {
                String table = "CREATE TABLE test" + i + "(" + cols + ")";
                stmt.executeUpdate(table);
            }
        }

        DatabaseMetaData metadata = this.conn.getMetaData();
        try (Statement stmt = this.conn.createStatement();
                ResultSet res = metadata.getTables(null, null, "%", new String[]{"TABLE"});) {
            String view = "CREATE OR REPLACE VIEW test AS ";
            res.next();
            while (true) {
                String table = res.getString(3);
                view = view + "SELECT * FROM " + table;

                if (res.next()) {
                    view = view + " UNION ";
                } else {
                    break;
                }
            }
            stmt.executeUpdate(view);
        }

        if (!this.conn.getAutoCommit()) {
            this.conn.commit();
        }
    }

    public Integer populate(Integer start, Integer end, Integer interval) throws SQLException {
        LocalDateTime curdate, olddate, enddate;
        Integer segment;
        Integer counter;
        Integer years;

        segment = 1;
        counter = 1;

        olddate = LocalDateTime.of(start, Month.JANUARY, 01, 0, 0);
        curdate = LocalDateTime.of(start, Month.JANUARY, 01, 0, 0);
        enddate = LocalDateTime.of(end, Month.DECEMBER, 31, 23, 0);

        for (; curdate.getYear() < enddate.getYear(); curdate = curdate.plusHours(1)) {

            if (curdate.getYear() - olddate.getYear() == interval) {
                if (!this.conn.getAutoCommit()) {
                    this.conn.commit();
                }

                if (segment != segments) {
                    segment++;
                } else {
                    segment = 1;
                }
                olddate = curdate;
            }

            String insert = "INSERT INTO test" + segment + " VALUES(?, ?, ?, ?)";
            try (PreparedStatement pstmt = this.conn.prepareStatement(insert);) {
                pstmt.setInt(1, counter);
                pstmt.setInt(2, Integer.parseInt(Integer.toString(curdate.getYear())
                        + String.format("%02d", curdate.getMonthValue())
                        + String.format("%02d", curdate.getDayOfMonth())));
                pstmt.setInt(3, Integer.parseInt(
                        String.format("%02d", curdate.getHour())
                        + String.format("%02d", curdate.getMinute())));
                pstmt.setString(4, "Inserted date " + curdate);
                pstmt.executeUpdate();
            }
            counter++;
        }

        if (!this.conn.getAutoCommit()) {
            this.conn.commit();
        }
        return counter;
    }
}
