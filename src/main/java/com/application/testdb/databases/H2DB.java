/*
 Copyright (C) 2015 Enrico Bianchi (enrico.bianchi@ymail.com)
 Project       TestDB
 Description   Simple database testing tool
 License       GPL version 2 (see GPL.txt for details)
 */
package com.application.testdb.databases;

import com.application.testdb.Main;
import com.application.testdb.databases.schema.Tables;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import org.ini4j.Wini;

/**
 *
 * @author enrico
 */
public class H2DB implements AutoCloseable {

    private Connection conn;
    private Integer segments;

    private Integer yearstart;
    private Integer yearend;
    private Integer interval;

    public H2DB(Wini cfg) throws ClassNotFoundException, SQLException {

        this.segments = Integer.parseInt(cfg.get("general", "tables"));
        this.yearstart = Integer.parseInt(cfg.get("years", "start"));
        this.yearend = Integer.parseInt(cfg.get("years", "end"));
        this.interval = Integer.parseInt(cfg.get("years", "interval"));

        this.openConnection(cfg.get("h2", "host"),
                cfg.get("h2", "database"),
                cfg.get("h2", "user"),
                cfg.get("h2", "password"),
                Boolean.parseBoolean(cfg.get("general", "autocommit")));
    }

    private void openConnection(String host, String dbname, String user, String password, Boolean autocommit)
            throws ClassNotFoundException, SQLException {

        String URL = "jdbc:h2:tcp://" + host + "/" + dbname;

        Class.forName("org.h2.Driver");
        this.conn = DriverManager.getConnection(URL, user, password);
        this.conn.setAutoCommit(autocommit);
    }

    @Override
    public void close() throws SQLException {
        this.conn.commit();
        this.conn.close();
    }

    public void test() throws SQLException {
        Integer records;
        Tables tables;

        tables = new Tables(this.conn, this.segments);
        if (!tables.checkSchema()) {
            Main.LOGGER.finest("Creating schema");
            tables.generate();
            Main.LOGGER.finest("Schema created");
            Main.LOGGER.finest("Populating schema");
            records = tables.populate(this.yearstart, this.yearend, this.interval);
            Main.LOGGER.log(Level.FINEST, "Populated schema. Inserted {0} records", records);
        }
    }
}
