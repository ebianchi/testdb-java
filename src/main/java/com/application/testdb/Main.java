/*
 Copyright (C) 2015 Enrico Bianchi (enrico.bianchi@ymail.com)
 Project       TestDB
 Description   Simple database testing tool
 License       GPL version 2 (see GPL.txt for details)
 */
package com.application.testdb;

import com.application.testdb.databases.H2DB;
import com.application.testdb.databases.PgDB;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.ini4j.Wini;

/**
 *
 * @author enrico
 */
public class Main {

    private Options opts;
    private Wini cfg;

    public final static Logger LOGGER = Logger.getLogger("TestDB");

    public Main() {
        this.opts = new Options();

        this.opts.addOption("h", "help", false, "Print this help");
        this.opts.addOption(OptionBuilder
                .withLongOpt("cfg")
                .withDescription("Load configuration file")
                .hasArg()
                .withArgName("CFG")
                .create("c"));
        this.opts.addOption(OptionBuilder
                .withLongOpt("postgresql")
                .withDescription("Test PostgreSQL")
                .create());
        this.opts.addOption(OptionBuilder
                .withLongOpt("h2")
                .withDescription("Test H2")
                .create());
    }

    private void setCfg(String cfgFile) {
        try {
            this.cfg = new Wini();
            this.cfg.load(new FileInputStream(cfgFile));
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(2);
        }
    }

    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("TestDB", this.opts);
        System.exit(0);
    }

    public void go(String[] args) throws ParseException, ClassNotFoundException, SQLException {
        CommandLine cmd;
        CommandLineParser parser;

        parser = new PosixParser();
        cmd = parser.parse(this.opts, args);
        if (cmd.hasOption("h") || cmd.hasOption("help")) {
            this.printHelp();
        }

        Handler console = new ConsoleHandler();
        console.setLevel(Level.FINEST);

        Main.LOGGER.addHandler(console);
        Main.LOGGER.setLevel(Level.FINEST);

        if (!cmd.hasOption("cfg")) {
            System.out.println("ERROR: Configuration file not found");
            System.exit(1);
        } else {
            this.setCfg(cmd.getOptionValue("cfg"));
        }

        Main.LOGGER.log(Level.FINEST, "Test started");
        if (cmd.hasOption("postgresql")) {
            try (PgDB db = new PgDB(this.cfg)) {
                db.test();
            }
        } else if (cmd.hasOption("h2")) {
            try (H2DB db = new H2DB(this.cfg);) {
                db.test();
            }
        } else {
        }
        Main.LOGGER.log(Level.FINEST, "Test ended");
    }

    public static void main(String[] args) {
        Main m;

        m = new Main();

        if (args.length == 0) {
            m.printHelp();
        }

        try {
            m.go(args);
        } catch (ParseException | ClassNotFoundException | SQLException ex) {
            Main.LOGGER.severe(ex.toString());
            System.exit(2);
        }
    }
}
